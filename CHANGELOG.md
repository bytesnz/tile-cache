# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.1.4] - 2023-03-19

### Added

- Added logger to options typings

### Fixed

- Checking for internet connectivity was throwing if not connected

## [0.1.3] - 2022-06-02

### Added

- Server response time logging

## [0.1.2] - 2022-06-02

### Fixed

- Middleware issue not passing through requested tile from remote server
- Changed to AGPL-3.0 license

## [0.1.1] - 2022-06-02

### Fixed

- Fixed example code in README

## [0.1.0] - 2022-06-02

### Added

- Basic tile cache downloading
- Basic middleware

[0.1.4]: https://gitlab.com/bytesnz/tile-cacher/compare/v0.1.3...v0.1.4
[0.1.3]: https://gitlab.com/bytesnz/tile-cacher/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/bytesnz/tile-cacher/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/bytesnz/tile-cacher/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/bytesnz/tile-cacher/tree/v0.1.0
