export const options = {
	servers: [
		{
			id: 'opentopomap',
			url: 'https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png',
			activelyCache: [
				{
					northLat: -40.975513,
					southLat: -41.100311,
					westLon: 174.994183,
					eastLon: 175.148163,
					maxZoom: 13
				}
			],
			cache: [
				{
					northLat: -39.975513,
					southLat: -42.100311,
					westLon: 173.994183,
					eastLon: 176.148163,
					maxZoom: 13
				}
			],
			age: 168
		}
	],
	checkInterval: 20,
	downloadWaitTime: 1000,
	extension: 'png',
	folder: 'cache',
	logger: {
		log: (...args) => console.log(new Date().toISOString(), 'LOG', ...args),
		warn: (...args) => console.warn(new Date().toISOString(), 'WARN', ...args),
		debug: (...args) => console.debug(new Date().toISOString(), 'DEBUG', ...args),
		info: (...args) => console.info(new Date().toISOString(), 'INFO', ...args),
		error: (...args) => console.error(new Date().toISOString(), 'ERROR', ...args)
	}
};
