# tile-cacher <!=package.json version>

Node express-like middleware and downloader for map tile caches

[![pipeline status](https://gitlab.com/bytesnz/tile-cacher/badges/main/pipeline.svg)](https://gitlab.com/bytesnz/tile-cacher/commits/main)
[![tile-cacher on NPM](https://bytes.nz/b/tile-cacher/npm)](https://npmjs.com/package/tile-cacher)
[![license](https://bytes.nz/b/tile-cacher/custom?color=yellow&name=license&value=AGPL-3.0)](https://gitlab.com/bytesnz/tile-cacher/blob/main/LICENSE)
[![developtment time](https://bytes.nz/b/tile-cacher/custom?color=yellowgreen&name=development+time&value=~8+hours)](https://gitlab.com/bytesnz/tile-cacher/blob/main/.tickings)
[![contributor covenant](https://bytes.nz/b/tile-cacher/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](https://gitlab.com/bytesnz/tile-cacher/blob/main/CODE_OF_CONDUCT.md)
[![support development](https://bytes.nz/b/tile-cacher/custom?color=brightgreen&name=support+development&value=$$)](https://liberapay.com/MeldCE)

## Middleware

The middleware can be used with HTTP libraries like [Express][] and [Polka][].
When instantiated, if connected to the Internet, it will check the actively
cached tile have been cached and remove old cached tiles. It will also
schedule the next check inline with the `checkInterval` option.

### Example

#### _examples/middleware.js_

```js
<!=!examples/middleware.js>
```

</details>

## Tile Cache Check

### Example

#### _examples/cacheCheck.js_

```js
<!=!examples/cacheCheck.js>
```

## Options

Options are the same for both the middlware and the cacheCheck functionality

### Example

#### _examples/options.js_

```js
<!=!examples/options.js>
```

## Development

Feel free to contribute to the code or documentation, or leave issues or suggestions on the [issue tracker](<!=package.json bugs.url>) or [email](mailto:<!=package.json bugs.email>) them to us. **Please submit security concerns as a [confidential issue](<!=package.json bugs.url>?issue[confidential]=true)**.

The source is hosted on [Gitlab](https://gitlab.com/bytesnz/tile-cacher) and
uses [prettier][], [lint-staged][] and [husky][] to keep things pretty. As such, when you first [clone][git-clone] the repository, as well as installing the npm dependencies, you will also need to install [husky][].

```bash
# Install NPM dependencies
npm install
# Set up husky Git hooks stored in .husky
npx husky install
```

<!=CHANGELOG.md>

[express]: http://expressjs.com/
[polka]: https://github.com/lukeed/polka#readme
[husky]: https://typicode.github.io/husky
[git-clone]: https://www.git-scm.com/docs/git-clone
[prettier]: https://prettier.io/
[lint-staged]: https://github.com/okonet/lint-staged#readme
