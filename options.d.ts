export interface Bounds {
	/// North-most latitude of bounds
	northLat: number;
	/// South-most latitude of bounds
	southLat: number;
	/// West-most longitude of bounds
	westLon: number;
	/// East-most longitude of bounds
	eastLon: number;
	/// Interval time between checking tiles are up-to-date
	lifetime?: number;
	/// Max zoom level to cache, default 20
	maxZoom?: number;
	/// Min zoom level to cache, default 0
	minZoom?: number;
}

export interface Server {
	/// ID of server. Will be used for url and folder used to store cache
	id: string;
	/// URL template of tiles
	url: string;
	/// Available subdomains for the url
	subdomains?: string;
	/// Time in seconds to keep cached tiles for
	age?: number;
	/// Bounds to pre-cache
	activelyCache?: Bounds[];
	/// Bounds to cache as accessed, if none given, no cache will be kept
	cache?: Bounds[];
}

export interface Options {
	/// Configs for servers to cache files from
	servers: Server[];
	/// Folder to cache files in (default ./cache)
	folder: string;
	/// Domain to use for testing internet connectivity (default google.com)
	testDomain: string;
	/// Interval to check for internet connectivity when not currently connected
	testInterval: number;
	/// Extension for the tile images (default png)
	extension?: string;
	/** Interval to check cache and delete/redownload and required tiles
	 *  in hours
	 */
	checkInterval?: number;
	/// Whether or not to use Etags
	useEtags?: boolean;
	/// Time to wait inbetween download jobs in milliseconds
	downloadWaitTime?: number;
	/// Logger to use for logging messages
	logger?: {
		error: (...args: any[]) => void;
		warn: (...args: any[]) => void;
		log: (...args: any[]) => void;
		info: (...args: any[]) => void;
		debug: (...args: any[]) => void;
	};
}
