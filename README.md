# tile-cacher 0.1.4

Node express-like middleware and downloader for map tile caches

[![pipeline status](https://gitlab.com/bytesnz/tile-cacher/badges/main/pipeline.svg)](https://gitlab.com/bytesnz/tile-cacher/commits/main)
[![tile-cacher on NPM](https://bytes.nz/b/tile-cacher/npm)](https://npmjs.com/package/tile-cacher)
[![license](https://bytes.nz/b/tile-cacher/custom?color=yellow&name=license&value=AGPL-3.0)](https://gitlab.com/bytesnz/tile-cacher/blob/main/LICENSE)
[![developtment time](https://bytes.nz/b/tile-cacher/custom?color=yellowgreen&name=development+time&value=~8+hours)](https://gitlab.com/bytesnz/tile-cacher/blob/main/.tickings)
[![contributor covenant](https://bytes.nz/b/tile-cacher/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](https://gitlab.com/bytesnz/tile-cacher/blob/main/CODE_OF_CONDUCT.md)
[![support development](https://bytes.nz/b/tile-cacher/custom?color=brightgreen&name=support+development&value=$$)](https://liberapay.com/MeldCE)

## Middleware

The middleware can be used with HTTP libraries like [Express][] and [Polka][].
When instantiated, if connected to the Internet, it will check the actively
cached tile have been cached and remove old cached tiles. It will also
schedule the next check inline with the `checkInterval` option.

### Example

#### _examples/middleware.js_

```js
import { createReadStream } from 'fs';
import polka from 'polka';
import middleware from 'tile-cacher';
import { options } from './options.js';
import { dirname, join } from 'path';
import { fileURLToPath } from 'url';

const __dirname = dirname(fileURLToPath(import.meta.url));

const app = polka();

app.get('/', (req, res) => {
  const stream = createReadStream(join(__dirname, 'index.html'));
  res.setHeader('Content-Type', 'text/html');
  stream.pipe(res);
});
app.get('/tiles/:id/:z/:x/:y.png', middleware(options));

app.listen(3030, () => {
  console.log('tile-cacher middleware listening on port 3030');
});

```

</details>

## Tile Cache Check

### Example

#### _examples/cacheCheck.js_

```js
import { checkCache } from 'tile-cacher';
import { options } from './options.js';

checkCache(options);

```

## Options

Options are the same for both the middlware and the cacheCheck functionality

### Example

#### _examples/options.js_

```js
export const options = {
  servers: [
    {
      id: 'opentopomap',
      url: 'https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png',
      activelyCache: [
        {
          northLat: -40.975513,
          southLat: -41.100311,
          westLon: 174.994183,
          eastLon: 175.148163,
          maxZoom: 13
        }
      ],
      cache: [
        {
          northLat: -39.975513,
          southLat: -42.100311,
          westLon: 173.994183,
          eastLon: 176.148163,
          maxZoom: 13
        }
      ],
      age: 168
    }
  ],
  checkInterval: 20,
  downloadWaitTime: 1000,
  extension: 'png',
  folder: 'cache',
  logger: {
    log: (...args) => console.log(new Date().toISOString(), 'LOG', ...args),
    warn: (...args) => console.warn(new Date().toISOString(), 'WARN', ...args),
    debug: (...args) => console.debug(new Date().toISOString(), 'DEBUG', ...args),
    info: (...args) => console.info(new Date().toISOString(), 'INFO', ...args),
    error: (...args) => console.error(new Date().toISOString(), 'ERROR', ...args)
  }
};

```

## Development

Feel free to contribute to the code or documentation, or leave issues or suggestions on the [issue tracker](https://gitlab.com/bytesnz/tile-cacher/-/issues) or [email](mailto:contact-project+bytesnz-tile-cacher-35748248-issue-@incoming.gitlab.com) them to us. **Please submit security concerns as a [confidential issue](https://gitlab.com/bytesnz/tile-cacher/-/issues?issue[confidential]=true)**.

The source is hosted on [Gitlab](https://gitlab.com/bytesnz/tile-cacher) and
uses [prettier][], [lint-staged][] and [husky][] to keep things pretty. As such, when you first [clone][git-clone] the repository, as well as installing the npm dependencies, you will also need to install [husky][].

```bash
# Install NPM dependencies
npm install
# Set up husky Git hooks stored in .husky
npx husky install
```

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.1.4] - 2023-03-19

### Added

- Added logger to options typings

### Fixed

- Checking for internet connectivity was throwing if not connected

## [0.1.3] - 2022-06-02

### Added

- Server response time logging

## [0.1.2] - 2022-06-02

### Fixed

- Middleware issue not passing through requested tile from remote server
- Changed to AGPL-3.0 license

## [0.1.1] - 2022-06-02

### Fixed

- Fixed example code in README

## [0.1.0] - 2022-06-02

### Added

- Basic tile cache downloading
- Basic middleware

[0.1.4]: https://gitlab.com/bytesnz/tile-cacher/compare/v0.1.3...v0.1.4
[0.1.3]: https://gitlab.com/bytesnz/tile-cacher/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/bytesnz/tile-cacher/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/bytesnz/tile-cacher/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/bytesnz/tile-cacher/tree/v0.1.0


[express]: http://expressjs.com/
[polka]: https://github.com/lukeed/polka#readme
[husky]: https://typicode.github.io/husky
[git-clone]: https://www.git-scm.com/docs/git-clone
[prettier]: https://prettier.io/
[lint-staged]: https://github.com/okonet/lint-staged#readme
