import { join } from 'path';
import { stat, mkdir } from 'fs/promises';

/* @typedef {import('./options.d.ts').Options Options */
/* @typedef {import('./options.d.ts').Bounds Bounds */

/**
 * Create a Generator of tile URI parts to cover the given bounds
 *
 * @param {Bounds} bounds Bounds to create the array for
 */
export function* makeTileList(bounds) {
	let maxZoom = bounds.maxZoom || 16;

	for (let z = bounds.minZoom || 0; z < maxZoom; z++) {
		let tile;
		const generator = makeTileZoomLevelList(bounds, z);
		while ((tile = generator.next())) {
			yield tile;
		}
	}
}

/**
 * Create a Generator of tile URI parts to cover the given bounds at the
 * given zoom level
 *
 * @param {Bounds} bounds Bounds to create the array for
 */
export function* makeTileZoomLevelList(bounds, zoom) {
	const { firstX, lastX, firstY, lastY } = getTileLimits(bounds, zoom);

	for (let x = firstX; x <= lastX; x++) {
		for (let y = firstY; y <= lastY; y++) {
			yield { z: zoom, x, y };
		}
	}
}

/**
 * Convert a longitude and zoom level to a tile x value
 */
export const lon2tile = (lon, z) => Math.floor(((lon + 180) / 360) * Math.pow(2, z));

/**
 * Convert a longitude and zoom level to a tile y value
 */
export const lat2tile = (lat, z) =>
	Math.floor(
		((1 -
			Math.log(Math.tan((lat * Math.PI) / 180) + 1 / Math.cos((lat * Math.PI) / 180)) / Math.PI) /
			2) *
			Math.pow(2, z)
	);

/**
 * Convert a tile x and zoom level to a longitude
 */
export const tile2lon = (x, z) => (x / Math.pow(2.0, z)) * 360.0 - 180;

/**
 * Convert a tile y and zoom level to a latitude
 */
export const tile2lat = (y, z) =>
	(Math.atan(Math.sinh(Math.PI - (2.0 * Math.PI * y) / Math.pow(2.0, z))) * 180) / Math.PI;

/**
 * Get the bounding latitude and longitude of a tile
 *
 * @param {number} z Zoom value of the tile
 * @param {number} x x value of the tile
 * @param {number} y y value of the tile
 */
export const getTileBounds = (z, x, y) => ({
	northLat: tile2lat(y, z),
	southLat: tile2lat(y + 1, z),
	westLon: tile2lon(x, z),
	eastLon: tile2lon(x + 1, z)
});

/**
 * Get the tile number limits for a given bounds and zoom level
 *
 * @param {Bounds} bounds Bounds to return the limits for
 * @param {number} zoom Zoom level
 */
export const getTileLimits = (bounds, zoom) => {
	return {
		firstX: lon2tile(bounds.westLon, zoom),
		lastX: lon2tile(bounds.eastLon, zoom),
		firstY: lat2tile(bounds.northLat, zoom),
		lastY: lat2tile(bounds.southLat, zoom)
	};
};

/**
 * Generator for generating tile limits (first and last tiles for each zoom
 * level) for a given bounds
 *
 * @param {Bounds} bounds Bounds to generate tile limits for
 */
export function* iterateTileLimits(bounds) {
	let maxZoom;
	if (typeof bounds.maxZoom === 'number') {
		maxZoom = Math.max(0, bounds.maxZoom);
	} else {
		maxZoom = 17;
	}

	for (let z = bounds.minZoom || 0; z <= maxZoom; z++) {
		yield {
			z,
			...getTileLimits(bounds, z)
		};
	}
}

/**
 * Generator for generating the number of tiles for each zoom level
 * for a given bounds
 *
 * @param {Bounds} bounds Bounds to generate tile numbers for
 */
export function* iterateTileNumbers(bounds) {
	let maxZoom;
	if (typeof bounds.maxZoom === 'number') {
		maxZoom = Math.max(0, bounds.maxZoom);
	} else {
		maxZoom = 17;
	}

	for (let z = bounds.minZoom || 0; z <= maxZoom; z++) {
		const limits = getTileLimits(bounds, z);

		yield { z, x: limits.lastX - limits.firstX, y: limits.lastY - limits.firstY };
	}
}

/**
 * Make the url for the given tile on the given server
 *
 * @param {Server} server Server config
 * @param {Tile} tile Tile to make url for
 */
export const makeTileUrl = (server, tile) => {
	const subdomains = server.subdomains?.split('') || ['a', 'b', 'c'];
	tile = {
		s: subdomains[Math.floor(Math.random() * subdomains.length)],
		...tile
	};
	return server.url.replace(/\{([sxyz])\}/g, (match, p1) => tile[p1]);
};

/**
 * Make the filepath for the given tile and server
 *
 * @param {Server} server Server config
 * @param {number} z Tile Z value
 * @param {number} x Tile X value
 * @param {number} y Tile Y value
 * @param {Options} options Options
 * @param {string} ext Extension to use for the file
 */
export const makeTileFilepath = (server, z, x, y, options, ext) =>
	join(options.folder, server.id, z, x, y + '.' + (ext || options.extension));

/**
 * Ensure the give folder exists. If it doesn't, create it
 *
 * @param {string} folder Folder to ensure exists
 */
export const ensureFolder = async (folder) => {
	try {
		const folderStat = await stat(folder);
		if (!folderStat.isDirectory()) {
			throw new Error(`Folder ${folder} is not a folder`);
		}
	} catch (error) {
		if (error.code === 'ENOENT') {
			await mkdir(folder, {
				recursive: true
			});
		} else {
			throw error;
		}
	}
};

/* eslint-disable no-console */
/**
 * Fill the given options with the default values
 *
 * @param {Options} options Options to fill
 */
export const fillOptionDefaults = (options) => ({
	logger: {
		log: (...args) => console.log(...args),
		warn: (...args) => console.warn(...args),
		debug: () => void 1,
		info: (...args) => console.info(...args),
		error: (...args) => console.error(...args)
	},
	useEtags: false,
	checkInterval: 24,
	downloadWaitTime: 1000,
	extension: 'png',
	folder: './cache',
	...(options || {}),
	servers: [...(options.servers || [])]
});
/* eslint-enable no-console */
