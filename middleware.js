import { dirname } from 'path';
import { stat } from 'fs/promises';
import { PassThrough } from 'stream';
import {
	ensureFolder,
	makeTileUrl,
	fillOptionDefaults,
	getTileBounds,
	makeTileFilepath
} from './utils.js';
import { checkCache, whenConnected } from './cacher.js';
import fetch from 'node-fetch';
import { createReadStream, writeFile } from 'fs';
import { fileTypeStream } from 'file-type';

/**
 * Create a tile-cacher middlware function
 *
 * @param {Options} [options] Options
 */
export default (options) => {
	options = fillOptionDefaults(options);

	const L = options.logger;

	L.debug('Using options', options);

	const doCheckCache = () => {
		whenConnected(() => {
			checkCache(options);
			if (options.checkInterval) {
				setTimeout(doCheckCache, options.checkInterval * 3600000);
			}
		}, options);
	};

	doCheckCache();

	return async (req, res, next) => {
		if (req.params) {
			const { id, x, y, z } = req.params;
			const ext = req.params.ext || options.extension;
			L.debug('request for tile', id, z, x, y);
			// Find server
			let server = null;
			for (let i = 0; i < options.servers.length; i++) {
				if (options.servers[i].id === id) {
					server = options.servers[i];
					break;
				}
			}

			if (!server) {
				L.info('found no server for', id);
				if (typeof next === 'function') next();
				return;
			}

			// Hack in support for expressjs header setting
			if (!res.setHeader && res.set) {
				res.setHeader = res.set;
			}

			const file = makeTileFilepath(server, z, x, y, options, ext);
			try {
				L.debug('checking for file', file);
				const fileStat = await stat(file);
				L.info('using cached file', file);
				const fileStream = createReadStream(file);
				const stream = await fileTypeStream(fileStream);
				if (server.age) {
					const date = new Date(fileStat.mtime);
					date.setHours(date.getHours() + server.age);
					res.setHeader('Expires', date.toUTCString());
				}
				res.setHeader('Content-Type', stream.fileType.mime);
				res.setHeader('Content-Size', fileStat.size);
				stream.pipe(res);
				if (typeof next === 'function') next();
				return;
			} catch (error) {
				if (error.code !== 'ENOENT') {
					L.error('Error checking for cached file', file, error);
				}
				const url = makeTileUrl(server, { z, x, y });
				L.debug('fetching from server', url);
				let response;
				const startTime = new Date().getTime();
				try {
					response = await fetch(url);
				} catch (error) {
					L.error('Got error making fetch request', error.message);
					return;
				}
				if (response.status === 200) {
					const size = response.size || Number(response.headers.get('Content-length'));
					if (size) res.setHeader('Content-Length', size);
					res.setHeader('Content-Type', response.headers.get('Content-Type'));
					if (response.headers.get('Expires')) {
						res.setHeader('Expires', response.headers.get('Expires'));
					}
					/*TODO Etag handling
          if (response.headers.get('Etag')) {
            res.setHeader('Etag', response.headers.get('Etag'));
          }
          */
					var converter = new PassThrough();

					// We'll store all the data inside this array
					converter.data = [];
					converter._transform = function (chunk, encoding, callback) {
						converter.data.push(chunk);
						callback(null, chunk);
					};

					// Will be emitted when the input stream has ended,
					// i.e. no more data will be provided
					converter.on('finish', function () {
						// Create a buffer from all the received chunks
						var b = Buffer.concat(this.data);

						// Check if should be cached
						if (server.cache) {
							const bounds = getTileBounds(z, x, y);

							for (let i = 0; i < server.cache.length; i++) {
								const cacheBounds = server.cache[i];
								if (
									((bounds.northLat <= cacheBounds.northLat &&
										bounds.northLat >= cacheBounds.southLat) ||
										(bounds.southLat <= cacheBounds.northLat &&
											bounds.southLat >= cacheBounds.southLat)) &&
									((bounds.eastLon <= cacheBounds.eastLon &&
										bounds.eastLon >= cacheBounds.westLon) ||
										(bounds.westLon <= cacheBounds.eastLon &&
											bounds.westLon >= cacheBounds.westLon))
								) {
									L.debug('caching fetched tile', file);
									// Cache the tile
									ensureFolder(dirname(file)).then(() => {
										writeFile(file, b, () => {});
									});
									break;
								}
							}
						}
					});

					L.debug(`Passing response for ${url} (took ${new Date().getTime() - startTime}ms)`);
					response.body.pipe(converter).pipe(res);
					if (typeof next === 'function') next();
				}
				return;
			}
		}
		res.end();
		return;
	};
};
