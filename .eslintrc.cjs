module.exports = {
	root: true,
	extends: ['eslint:recommended', 'prettier'],
	parserOptions: {
		sourceType: 'module',
		ecmaVersion: 2020
	},
	env: {
		browser: true,
		es2017: true,
		node: true
	},
	rules: {
		'no-console': [1, { allow: ['warn', 'error'] }],
		'no-debugger': 1,
		'no-warning-comments': [
			1,
			{ terms: ['xxx', 'todo', 'fixme', 'todo!!!'], location: 'anywhere' }
		],
		'require-jsdoc': 1
	},
	overrides: [
		{
			files: ['examples/*.js'],
			rules: {
				'no-console': 0
			}
		}
	]
};
