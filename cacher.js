import fetch from 'node-fetch';
import { stat, mkdir, unlink } from 'fs/promises';
import { createWriteStream } from 'fs';
import { join, extname } from 'path';
import { promises as dnsPromises } from 'dns';
const { resolve } = dnsPromises;
import {
	ensureFolder,
	makeTileUrl,
	iterateTileLimits,
	iterateTileNumbers,
	getTileLimits,
	fillOptionDefaults
} from './utils.js';

/* @typedef {import('./options.d.ts').Options} Options */
/* @typedef {import('./options.d.ts').Boundss} Bounds */

const downloadQueue = [];
const downloading = [];

// TODO Add maxDownloads to options
/**
 * Add URL to download queue
 *
 * @param {string} URL to download
 * @param {{[key: string]: value}} headers Headers to include in request
 * @param {Options} options Options
 *
 * @returns {Promise} A Promise that resolves once the file has been
 *   downloaded
 */
const addToDownloadQueue = (url, headers, destination, options) => {
	const job = { url, headers, destination };
	if (downloading.length < 5) {
		return downloadJob(job, options);
	}

	return new Promise((resolve) => {
		job.resolve = resolve;

		downloadQueue.push(job);
	});
};

/**
 * Job downloader function. Will go through download queue and download
 * items in queue
 *
 * @param {Job} job Job to start with
 * @params {Options} options
 */
const downloadJob = async (job, options) => {
	downloading.push(job);
	options.logger.debug(`Downloading ${job.url} to ${job.destination}`);
	const response = await fetch(job.url, {
		headers: job.headers || null
	});

	if (response.status === 304) {
		return;
	} else if (response.status === 200) {
		const stream = createWriteStream(job.destination);
		response.body.pipe(stream);
	} else {
		// TODO
	}

	if (job.resolve) {
		job.resolve();
	}

	const index = downloading.indexOf(job);

	if (index !== -1) {
		downloading.splice(index, 1);
	}

	if (downloadQueue.length) {
		const newJob = downloadQueue.shift();
		if (downloadQueue.length % 20 === 0) {
			options.logger.log(`${downloadQueue.length} left in queue`);
		}
		setTimeout(() => {
			downloadJob(newJob, options);
		}, options.downloadWaitTime || 1000);
	}
};

/**
 * Run the callback when an connection to the internet is detected
 *
 * @param {function} callback Callback to run when a connection to the
 *   Internet has been detected
 * @param {Options} [options] Options
 */
export const whenConnected = (callback, options) => {
	resolve(options?.testDomain || 'google.com').then(
		() => {
			callback();
		},
		(error) => {
			options.logger.debug('Received error when resolving DNS:', error.message);
			setTimeout(() => {
				whenConnected(callback, options);
			}, options.testInterval || 10000);
		}
	);
};

/**
 * Check the cache for the given bounds, zoom level and server
 *
 * @params {Bounds} bounds Bounds to check cache for
 * @params {number} zoom Zoom level to check cache for
 * @params {Server} server Server config to check cache for
 * @params {string} folder Folder of cache
 * @params {Options} options Options
 */
const downloadZoomLevel = async (bounds, zoom, server, folder, options) => {
	const limits = getTileLimits(bounds, zoom);
	options.logger.debug(
		'calculated tile limits',
		limits,
		'for bounds',
		bounds,
		'and zoom level',
		zoom
	);
	for (let x = limits.firstX; x <= limits.lastX; x++) {
		const tileFolder = join(folder, x.toString());
		await ensureFolder(tileFolder);

		for (let y = limits.firstY; y <= limits.lastY; y++) {
			const url = makeTileUrl(server, { z: zoom, x, y });
			const file = join(tileFolder, y + extname(url));

			const headers = {};
			try {
				const fileStat = await stat(file);
				// Skip download if not older than configured age
				if (
					server.age &&
					(new Date().getTime() - fileStat.mtime.getTime()) / 3600000 <= server.age
				) {
					continue;
				}
				headers['if-modified-since'] = fileStat.mtime.toUTCString();
			} catch (error) {
				if (error.code === 'ENOENT') {
					void 1;
				}
			}

			addToDownloadQueue(url, headers, file, options);
		}
	}
};

/**
 * Check the cache
 *
 * @param {Options} options Options
 */
export const checkCache = async (options) => {
	options = fillOptionDefaults(options);
	// Check the cache folder exists
	try {
		const folderStat = await stat(options.folder);
		if (!folderStat.isDirectory()) {
			throw new Error('Cache folder is not a folder');
		}
	} catch (error) {
		if (error.code === 'ENOENT') {
			await mkdir(options.folder);
		} else {
			throw error;
		}
	}

	// Go through each of the servers and check caches
	for (let i = 0; i < options.servers.length; i++) {
		const server = options.servers[i];
		const folder = join(options.folder, server.id);
		options.logger.info(`Checking caches for ${server.id}`);

		// Check if the cache folder exists
		try {
			const folderStat = await stat(folder);
			if (!folderStat.isDirectory()) {
				throw new Error('Cache folder is not a folder');
			}
		} catch (error) {
			if (error.code === 'ENOENT') {
				await mkdir(folder);
			} else {
				throw error;
			}
		}

		// Check parts have been downloaded
		if (server.activelyCache) {
			for (let j = 0; j < server.activelyCache.length; j++) {
				const bounds = server.activelyCache[j];
				options.logger.debug(`Checking bounds ${server.id}`, bounds);

				const counts = iterateTileNumbers(bounds);

				let value;
				while ((value = counts.next())) {
					if (value.done) break;
					const level = value.value;
					const zoomFolder = join(folder, level.z.toString());
					// Check folder exists
					try {
						const folderStat = await stat(zoomFolder);
						if (!folderStat.isDirectory()) {
							throw new Error(`Folder for $(server.id}/${level.z} is not a folder`);
						}
						// TODO if number of files is correct
						downloadZoomLevel(bounds, level.z, server, zoomFolder, options);
					} catch (error) {
						if (error.code === 'ENOENT') {
							await mkdir(zoomFolder);
							downloadZoomLevel(bounds, level.z, server, zoomFolder, options);
						} else {
							throw error;
						}
					}
				}
			}
		}

		if (server.cache && server.age) {
			for (let j = 0; j < server.activelyCache.length; j++) {
				const bounds = server.activelyCache[j];
				options.logger.debug(`Checking bounds ${server.id}`, bounds);

				const limits = iterateTileLimits(bounds);

				let value;
				while ((value = limits.next())) {
					if (value.done) break;
					const level = value.value;
					const zoomFolder = join(folder, level.z.toString());
					// Check folder exists
					let folderStat;
					try {
						folderStat = await stat(zoomFolder);
						if (!folderStat.isDirectory()) {
							throw new Error(`Folder for $(server.id}/${level.z} is not a folder`);
						}
					} catch (error) {
						if (error.code === 'ENOENT') {
							continue;
						} else {
							throw error;
						}
					}

					for (let x = level.firstX; x <= level.lastX; x++) {
						const xFolder = join(folder, x.toString());
						// Check folder exists
						try {
							folderStat = await stat(xFolder);
							if (!folderStat.isDirectory()) {
								throw new Error(`Folder for $(server.id}/${level.z}/${x} is not a folder`);
							}
						} catch (error) {
							if (error.code === 'ENOENT') {
								continue;
							} else {
								throw error;
							}
						}

						for (let y = level.firstY; y <= level.lastY; y++) {
							const file = join(xFolder, y + options.extension);

							try {
								let fileStat = await stat(file);

								if ((new Date().getTime() - fileStat.mtime.getTime()) / 3600000 > server.age) {
									options.logger.debug('Removing old file', file);
									await unlink(file);
								}
							} catch (error) {
								if (error.code === 'ENOENT') {
									continue;
								} else {
									throw error;
								}
							}
						}
					}
				}
			}
		}
	}
};
